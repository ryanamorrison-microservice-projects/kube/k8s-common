k8s-common
=========

Install common dependencies for control plane and worker nodes alike. This role is designed to prepare a cluster of nodes (ec2, VM's, bare-metal, etc.) but it does not initialize the cluster. This series of roles is more purpose-specific than a general implementation like kube-spray.

Requirements
------------
Assumes certain ansible inventory groups are present in a specific hierarchy:
```
---
firstcp:
  hosts:
    cp1:
      var_host_endpoint_ip: 192.168.1.2
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
restcps:
  hosts:
    cp2:
      var_host_endpoint_ip: 192.168.1.3
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
allcps:
  children:
    firstcp:
    restcps:
  vars:
workers:
  hosts:
    worker1:
      var_host_endpoint_ip: 192.168.1.4
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
    worker2:
      var_host_endpoint_ip: 192.168.1.5
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
    worker3:
      var_host_endpoint_ip: 192.168.1.6
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
  vars:
allkube:
  children:
    allcps:
    workers:
```
Also see variables below

Role Variables
--------------
Are broken into two categories based on where they are located.
### default variables
In defaults/main.yml there is the kube version to install which dictates with repo is installed.
```
var_kube_version: 1.27
```
Whether to install additional packages to aid in debugging (mostly network utilities):
```
install_debug_packages: true
```
### inventory variables
each node (ec2 instance, host, QEMU vm, etc.) is assumed to have a ssh key that CI runners will use to connect to the host:
```
ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
```
each node needs to have an IP address on a L2 network that all other hosts are connected to, adjust as needed:
```
var_host_endpoint_ip: 192.168.1.6
```

Dependencies
------------
N/A

Example Playbook
----------------
```
---
- name: Converge
  hosts: all
  gather_facts: true
  become: yes
  become_method: sudo
  tasks:
    - name: "include k8s-common role"
      ansible.builtin.include_role:
        name: k8s-common
```
Or run `molecule test -s local-vagrant` (requires molecule, vagrant and virtualbox as configured)     

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
